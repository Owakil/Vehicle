<?php

include_once 'vendor/autoload.php';

use Pondit\Vehicle\LandVehicle\Car;
use Pondit\Vehicle\LandVehicle\Truck;

$car1 = new Car();
var_dump($car1);

$truck1 = new Truck();
var_dump($truck1);

use Pondit\Vehicle\AirVehicle\Plane;
use Pondit\Vehicle\AirVehicle\Helicopter;

$plane1 = new Plane();
var_dump($plane1);

$helicopter1 = new Helicopter();
var_dump($helicopter1);

use Pondit\Vehicle\WaterVehicle\Ship;

$ship1 = new Ship();
var_dump($ship1);




